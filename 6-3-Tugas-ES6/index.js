//SOAL 1
const luasPersegiPanjang = (panjang, lebar) => {
    return panjang * lebar
}

const kelilingPersegiPanjang = (panjang, lebar) => {
    return (
        2 * (panjang + lebar)
    )
}

console.log(luasPersegiPanjang(2, 7));
console.log(kelilingPersegiPanjang(2, 8));

//SOAL 2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + ' ' + lastName);
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()
// console.log(newFunction('William', 'Imoh'))


//SOAL 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject

console.log(firstName, lastName, address, hobby)


//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

const combined = [...west, ...east];
//Driver Code
console.log(combined)

//SOAL 5
const planet = "earth"
const view = "glass"
const before = `Lorem  ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)