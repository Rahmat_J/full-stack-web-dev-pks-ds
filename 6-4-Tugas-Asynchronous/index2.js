//Soal 2

var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
//Jawaban 2

readBooksPromise(10000, books[0])
    .then(function (sisa1) {
        readBooksPromise(sisa1, books[1])
            .then(function (sisa2) {
                readBooksPromise(sisa2, books[2])
                    .then(function (sisa3) {
                        readBooksPromise(sisa3, books[3])
                    })
            })
    })
    .catch(error => console.log(error));