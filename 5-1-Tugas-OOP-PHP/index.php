<?php


trait Hewan
{

    //property
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo  "{$this->nama}  sedang  {$this->keahlian}";
    }
}

abstract class Fight
{

    use Hewan;

    //property
    public $attackPower;
    public $defencePower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        echo "<br>";
        $hewan->diserang($this);
        echo "<br>";
    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackPower / $this->defencePower);
    }

    abstract public function getInfoHewan();


    protected function getInfo()
    {
        echo "Nama Hewan : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defence Power : {$this->defencePower}";
    }
}

class Elang extends Fight
{

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = 'Terbang Tinggi';
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Elang";
        echo "<br>";
        $this->getInfo();
    }
}

class Harimau extends Fight
{

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = 'Berlari Cepat';
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis Hewan : Harimau";
        echo "<br>";
        $this->getInfo();
    }
}

class Spasi
{
    public static function tampilkan()
    {
        echo "<br>";
        echo "<br>";
        echo "<br>";
    }
}

$harimau = new Harimau("harimau sumatera");
$harimau->getInfoHewan();
Spasi::tampilkan();

$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
Spasi::tampilkan();

$harimau->serang($elang);
Spasi::tampilkan();

$elang->getInfoHewan();
Spasi::tampilkan();

$elang->serang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();
