// SOAL 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var hewan = daftarHewan.sort();
for (var i = 0; i < hewan.length; i++) {
    console.log(hewan[i])
}

//SOAL 2

function introduce(item) {
    return `Nama saya ${item.name}, umur saya ${item.age} tahun, alamat saya di ${item.address}, dan saya punya hobby yaitu ${item.hobby}!`
}

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// SOAL 3

function hitung_huruf_vokal(huruf) {
    var text = huruf.toLowerCase()
    var result = text.match(/[aeiou]/g).length
    return result
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2


//SOAL 4

function hitung(number) {
    var hasil = number + (-2 + number)
    return hasil
}

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8