<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }


    protected $fillable = ['name', 'email', 'username', 'role_id', 'password', 'email_verified_at'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    // public function role()
    // {
    //     return $this->belongsTo('App\Role');
    // }

    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            if( empty($model-> {$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }

            // $model->role_id = Role::where('role', 'author')->first()->id;
        });
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
