<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
    protected $fillable = ['role', 'id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;


    protected static function boot()
    {
        parent::boot();

        static::creating( function($model){
            // if( empty($model-> {$model->getKeyName()})){
            //     $model->{$model->getKeyName()} = Str::uuid();
            // }

            if( empty($model->id)){
                $model->id = Str::uuid();
            }
        });
    }

}
